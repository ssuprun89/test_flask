from flask import Flask, render_template, request, jsonify

from formula import formula_1

app = Flask(__name__)


@app.route('/',  methods=['GET', 'POST'])
def hello_world():
    if request.method == 'GET':
        return render_template('index.html')
    if request.method == 'POST':
        data = request.form
        date_year = int(data['date'])
        count_people = int(data['count_people'])
        difference = int(data['date_difference'])
        resp = formula_1(date_year, count_people, difference)
        return jsonify({'response':resp})


if __name__ == '__main__':
    app.debug = True
    app.run()
