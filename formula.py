import argparse


def formula_1(year_date, count_p, difference):
    sum_result = 1
    for i in range(1, int(count_p)+1):
        sum_result *= 1 - ((difference*(i-1))/year_date)
    return 1 - sum_result


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-yd', type=int, help='days in year')
    parser.add_argument('-count_p', type=int, help='Count people in room')
    parser.add_argument('-d', type=int, help='difference between birthday')
    args = parser.parse_args()
    result = formula_1(args.yd, args.count_p, args.d)
    print('Result: {0} or ({1}%) '.format(result, result*100))
