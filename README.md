1. Задача написать формулу, которая будет считать вероятность в python-script
2. Задача была, написать элементарный flask-server, который будет считать это все на front
<hr>
Формула по которой я решил данное задание:<br>

**n** - количество людей в комнате<br>
**drange** - разница между днями рождениями<br>
**dyear** - количество дней в году
<br>

![alt text](static/readme/formula.png)

Что бы запустить python-script: ***python formula.py -yd 365 -count_p 4 -d 7***
<br>
Что бы запустить flask-server:  **<i>python app.py</i>**